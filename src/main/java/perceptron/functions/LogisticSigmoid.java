package perceptron.functions;

import static java.lang.Math.exp;

public class LogisticSigmoid implements ActivationFunction {

    public double value(double x) {
        return 1 / (1 + exp(-x));
    }

    public double derivative(double x) {
        double f = value(x);
        return f * (1 - f);
    }
}
