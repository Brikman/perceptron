package view;

import perceptron.functions.Functions;
import perceptron.network.Backpropagation;
import perceptron.network.NeuronNetwork;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class Form {

    private JPanel rootPanel;
    private JPanel settingsPanel;
    private JPanel actionPanel;
    private JButton clearButton;
    private JButton createButton;
    private JRadioButton outputRadio1;
    private JRadioButton outputRadio2;
    private JRadioButton outputRadio3;
    private JRadioButton outputRadio4;
    private JTextField outputField1;
    private JTextField outputField2;
    private JTextField outputField3;
    private JTextField outputField4;
    private JButton testButton;
    private JTextField hiddenLayersField;
    private JTextField learnStepField;
    private JPanel drawingPanel;
    private JButton learnButton;
    private JTextField receptorsLayerField;
    private JTextField iterationsField;
    private JTextField hiddenNeuronsField;
    private JButton loadButton;
    private java.util.List<JRadioButton> outputButtons = Arrays.asList(outputRadio1, outputRadio2, outputRadio3, outputRadio4);
    private java.util.List<JTextField>   outputFields  = Arrays.asList(outputField1, outputField2, outputField3, outputField4);

    private DrawingField  drawingField;
    private NeuronNetwork network;

    Path learnFile = Paths.get("D:/learn.txt");

    public Form() {
        createButton.addActionListener(e -> {
            int inputNeurons  = Integer.parseInt(receptorsLayerField.getText());
            int hiddenLayers  = Integer.parseInt(hiddenLayersField.getText());
            int hiddenNeurons = Integer.parseInt(hiddenNeuronsField.getText());
            int iterations    = Integer.parseInt(iterationsField.getText());
            double step       = Double.parseDouble(learnStepField.getText());
            int outputNeurons = 4;

            network = new NeuronNetwork(
                    inputNeurons * inputNeurons,
                    hiddenNeurons, hiddenLayers,
                    outputNeurons,
                    Functions.LOGISTIC_SIGMOID,
                    Backpropagation.byIterations(iterations, step)
            );

            drawingField = new DrawingField(drawingPanel.getGraphics(), drawingPanel.getWidth(), inputNeurons);
            drawingField.undrawAll();

            outputFields.forEach(s -> s.setText(""));
            outputRadio1.setSelected(true);
        });
        clearButton.addActionListener(e -> {
            if (drawingField != null) {
                drawingField.undrawAll();
            }
        });
        drawingPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (drawingField != null) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        drawingField.draw(e.getPoint());
                    } else if (e.getButton() == MouseEvent.BUTTON3) {
                        drawingField.undraw(e.getPoint());
                    }
                }
            }
        });
        drawingPanel.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (drawingField != null) {
                    drawingField.draw(e.getPoint());
                }
            }
        });
        testButton.addActionListener(e -> {
            if (drawingField != null) {
                double[] input = Arrays.stream(drawingField.getData())
                        .flatMapToDouble(Arrays::stream)
                        .toArray();

                double[] result = network.test(input);

                double max = Arrays.stream(result).max().getAsDouble();
                for (int i = 0; i < result.length; i++) {
                    outputFields.get(i).setText(String.format("%.6f", result[i]));
                    if (result[i] == max)
                        outputButtons.get(i).setSelected(true);
                }
            }
        });
        learnButton.addActionListener(e -> {
            if (drawingField != null) {
                double[] input = Arrays.stream(drawingField.getData())
                        .flatMapToDouble(Arrays::stream)
                        .toArray();
                double[] target = outputButtons.stream()
                        .mapToDouble(s -> (s.isSelected() ? 1 : 0))
                        .toArray();

                StringBuilder builder = new StringBuilder();
                Arrays.stream(input).forEach(s -> builder.append(s + " "));
                builder.append(": ");
                Arrays.stream(target).forEach(s -> builder.append(s + " "));
                builder.append("\n");

                try {
                    if (!Files.exists(learnFile))
                        Files.createFile(learnFile);
                    Files.write(learnFile, builder.toString().getBytes(), StandardOpenOption.APPEND);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                double[] result = network.learn(input, target);
                for (int i = 0; i < result.length; i++) {
                    outputFields.get(i).setText(String.format("%.6f", result[i]));
                }
            }
        });
        loadButton.addActionListener(e -> {
            if (Files.exists(learnFile)) {
                try (BufferedReader reader = new BufferedReader(new FileReader(learnFile.toFile()))) {
                    String[] line = reader.readLine().split(":");
                    String[] inp = line[0].trim().split(" ");
                    String[] trg = line[1].trim().split(" ");

                    double[] input = new double[inp.length];
                    for (int i = 0; i < input.length; i++)
                        input[i] = Double.parseDouble(inp[i]);
                    double[] target = new double[trg.length];
                    for (int i = 0; i < target.length; i++)
                        target[i] = Double.parseDouble(trg[i]);

                    double[] result = network.learn(input, target);
                    for (int i = 0; i < result.length; i++) {
                        outputFields.get(i).setText(String.format("%.6f", result[i]));
                    }

                    System.out.println(Arrays.toString(input));
                    System.out.println(Arrays.toString(target));
                    System.out.println();

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No learning file found");
            }
        });
    }

    public static void main(String[] args) {
        Form editor = new Form();
        JFrame frame = new JFrame("Network");
        frame.setContentPane(editor.rootPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
    }
}
