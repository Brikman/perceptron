package perceptron.network;

import perceptron.functions.Functions;

public class InputLayer extends Layer {
    private double[] input;

    public InputLayer(int numberNeurons) {
        super(numberNeurons, Functions.IDENTITY);
        neurons.clear();
        for (int i = 0; i < numberNeurons; i++)
            neurons.add(new StaticNeuron(ID + "-" + i, this, Functions.IDENTITY));
    }

    public void setInput(double[] input) {
        this.input = input;
        for (int i = 0; i < neurons.size(); i++) {
            neurons.get(i).setWeights(new double[] {1});
            ((StaticNeuron) neurons.get(i)).setInput(new double[] {input[i]});
        }
    }

    @Override
    public double[] getInput() {
        return input;
    }
}
