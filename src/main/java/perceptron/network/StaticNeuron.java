package perceptron.network;

import perceptron.functions.ActivationFunction;

public class StaticNeuron extends Neuron {
    private double[] input;

    public StaticNeuron(String id, Layer layer) {
        super(id, layer);
    }

    public StaticNeuron(String id, Layer layer, ActivationFunction activation) {
        super(id, layer, activation);
    }

    public StaticNeuron(String id, Layer layer, ActivationFunction activation, double shift) {
        super(id, layer, activation, shift);
    }

    public void setInput(double[] input) {
        this.input = input;
    }

    @Override
    public double computeOutput() {
        double sum = 0;
        for (int i = 0; i < weights.length; i++)
            sum += input[i] * weights[i];
        return output = activation.value(sum + shift);
    }
}
