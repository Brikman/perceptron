package perceptron.functions;

public class Functions {
    public static final IdentityFunction  IDENTITY = new IdentityFunction();
    public static final LogisticSigmoid LOGISTIC_SIGMOID = new LogisticSigmoid();
    public static final HyperbolicTangent HYPERBOLIC_TANGENT = new HyperbolicTangent();
}
