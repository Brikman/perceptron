package perceptron.network;

import java.util.List;

public class Backpropagation extends LearningAlgorithm {
    private enum Mode {
        PRECISION,
        ITERATIONS
    }

    private final Mode mode;
    private double step;
    private double precision;
    private int iterations;

    public static Backpropagation byPrecision(double precision, int maxIterations) {
        return new Backpropagation(precision, maxIterations);
    }

    public static Backpropagation byIterations(int iterations, double step) {
        return new Backpropagation(iterations, step);
    }

    public Backpropagation(int iterations, double step) {
        super();
        this.iterations = iterations;
        this.step = step;
        this.mode = Mode.ITERATIONS;
    }

    public Backpropagation(double precision, int maxIterations) {
        super();
        this.precision = precision;
        this.iterations = maxIterations;
        this.mode = Mode.PRECISION;
    }

    @Override
    public double[] learn(double[] input, double[] target) {
        double[] result = null;
        if (mode == Mode.ITERATIONS) {
            for (int i = 0; i < iterations; i++) {
                result = process(input, target);
            }
        } else if (mode == Mode.PRECISION) {
            int i = 0;
            do {
                result = process(input, target);
                i++;
            } while (stdev(result, target) > precision && i < iterations);
            System.out.println("TOTAL ITERATIONS: " + i);
        }
        return result;
    }

    private double[] process(double[] input, double[] target) {
        Layer inputLayer = network.getInputLayer();
        Layer outputLayer = network.getOutputLayer();
        List<Layer> hiddenLayers = network.getHiddenLayers();

        double[] result = network.test(input);
        // OUTPUT LAYER
        double[] errors = new double[outputLayer.size()];
        // FOR EACH NEURON
        for (int j = 0; j < outputLayer.size(); j++) {
            Neuron neuron = outputLayer.getNeurons().get(j);

            // COMPUTING OUTPUT ERROR
            double error = (target[j] - result[j]) * neuron.getActivation().derivative(neuron.getInput());
            errors[j] = error;
            // SHIFT CORRECTION
            neuron.setShift(neuron.getShift() + step * error);
            // WEIGHTS CORRECTION
            Layer prev = hiddenLayers.isEmpty() ? inputLayer : hiddenLayers.get(hiddenLayers.size() - 1);
            double[] weights = neuron.getWeights();
            for (int k = 0; k < neuron.getWeights().length; k++)
                weights[k] += step * error * prev.getOutput()[k];
        }

        // HIDDEN LAYERS
        if (hiddenLayers.size() > 0) {
            int numLayers = hiddenLayers.size();
            // FOR EACH LAYER
            for (int j = numLayers - 1; j >= 0; j--) {
                Layer current = hiddenLayers.get(j);
                Layer next = j == numLayers - 1 ? outputLayer : hiddenLayers.get(j + 1);
                Layer prev = j == 0 ? inputLayer : hiddenLayers.get(j - 1);
                double[] curErrors = new double[current.size()];
                // FOR EACH NEURON
                for (int k = 0; k < current.size(); k++) {
                    Neuron neuron = current.getNeurons().get(k);
                    // COMPUTING ERROR
                    // FOR EACH LINKED NEURON FROM NEXT LAYER
                    double error = 0;
                    for (int p = 0; p < next.size(); p++)
                        error += errors[p] * next.getNeurons().get(p).getWeights()[k];
                    error *= neuron.getActivation().derivative(neuron.getInput());
                    curErrors[k] = error;
                    // SHIFT CORRECTION
                    neuron.setShift(neuron.getShift() + step * error);
                    // WEIGHTS CORRECTION
                    double[] weights = neuron.getWeights();
                    for (int p = 0; p < neuron.getWeights().length; p++)
                        weights[p] += step * error * prev.getOutput()[p];
                    // FLUSH CURRENT ERRORS TO PREVIOUS LAYER
                    errors = curErrors;
                }
            }
        }
        return result;
    }

    private double stdev(double[] result, double[] target) {
        double stdev = 0;
        for (int i = 0; i < result.length; i++)
            stdev += Math.pow(result[i] - target[i], 2);
        return Math.sqrt(stdev / result.length);
    }
}
