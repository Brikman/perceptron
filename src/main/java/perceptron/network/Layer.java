package perceptron.network;

import perceptron.functions.ActivationFunction;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;

public class Layer implements Iterable<Neuron> {
    private static int count = 1;
    public final int ID;

    protected List<Neuron> neurons;
    protected Layer linkedLayer;

    protected double[] output;

    public Layer(int numberNeurons, ActivationFunction activation) {
        this.ID = count++;
        this.output = new double[numberNeurons];
        this.neurons = new ArrayList<>(numberNeurons);
        for (int i = 0; i < numberNeurons; i++)
            neurons.add(new Neuron(ID + "-" + i, this, activation));
    }

    public Layer(int numberNeurons, ActivationFunction activation, Layer layer) {
        this(numberNeurons, activation);
        this.linkedLayer = layer;
        int numberInputs = layer.getOutput().length;
        initWeights(-0.5, 0.5);
        setShift(-0.5, 0.5);
    }

    public void initWeights(double from, double to) {
        Random random = new Random();
        for (Neuron neuron : neurons) {
            neuron.setWeights(random.doubles(linkedLayer.getOutput().length, from, to).toArray());
        }
    }

    public void initWeights(double[] weights) {
        neurons.forEach(s -> s.setWeights(weights));
    }

    public void setLinkedLayer(Layer layer) {
        this.linkedLayer = layer;
    }

    public void setShift(double from, double to) {
        double shift = ThreadLocalRandom.current().nextDouble(from, to);
        neurons.forEach(s -> s.setShift(shift));
    }

    public double[] compute() {
        return output = neurons.stream().mapToDouble(Neuron::computeOutput).toArray();
    }

    public double[] getOutput() {
        return output;
    }

    public double[] getInput() {
        return linkedLayer != null ? linkedLayer.getOutput() : null;
    }

    public List<Neuron> getNeurons() {
        return neurons;
    }

    public Layer getLinkedLayer() {
        return linkedLayer;
    }

    public int size() {
        return neurons.size();
    }

    @Override
    public Iterator<Neuron> iterator() {
        return neurons.iterator();
    }

    @Override
    public void forEach(Consumer<? super Neuron> action) {
        neurons.forEach(action);
    }

    public String getWeightMatrix() {
        StringBuilder builder = new StringBuilder();
        builder.append("LAYER " + ID + " WEIGHTS:\n");
        for (Neuron neuron : neurons) {
            builder.append(neuron.ID + " [ ");
            double[] weights = neuron.getWeights();
            for (double weight : neuron.getWeights())
                builder.append(String.format("%8.4f ", weight));
            builder.append("]\n");
        }
        return builder.toString();
    }

    public String getOutputs() {
        StringBuilder builder = new StringBuilder();
        builder.append("LAYER " + ID + " OUTPUTS:\n    [ ");
        for (Neuron neuron : neurons)
            builder.append(String.format("%8.4f ", neuron.getOutput()));
        builder.append("]\n");
        return builder.toString();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LAYER " + ID + ":\n");
        if (!neurons.isEmpty())
            builder.append(String.format("shift: %.4f\n", neurons.get(0).getShift()));
        for (Neuron neuron : neurons) {
            builder.append(neuron.ID + " [ ");
            double[] weights = neuron.getWeights();
            for (double weight : neuron.getWeights())
                builder.append(String.format("%8.4f ", weight));
            builder.append(String.format("] => %.4f\n", neuron.getOutput()));
        }
        return builder.toString();
    }
}
