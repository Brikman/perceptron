package perceptron.functions;

public interface ActivationFunction {
    double value(double x);
    double derivative(double x);
}
