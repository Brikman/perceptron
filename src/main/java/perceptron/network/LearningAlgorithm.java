package perceptron.network;

import java.util.Objects;

public abstract class LearningAlgorithm {
    protected NeuronNetwork network;

    public LearningAlgorithm() {
    }

    public LearningAlgorithm(NeuronNetwork network) {
        this.network = network;
    }

    public void setNetwork(NeuronNetwork network) {
        this.network = Objects.requireNonNull(network);
    }

    public abstract double[] learn(double[] input, double[] target);
}
