package view;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Pixel {
    public int x;
    public int y;
    public int width;
    public int height;
    public boolean drawn;

    public Pixel(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width  = width;
        this.height = height;
        this.drawn  = false;
    }

    public void draw(Graphics2D graphics2D) {
        graphics2D.setColor(Color.BLACK);
        graphics2D.fill(new Rectangle2D.Double(x, y, width, height));
        drawn = true;
    }

    public void undraw(Graphics2D graphics2D) {
        graphics2D.setColor(Color.WHITE);
        graphics2D.fill(new Rectangle2D.Double(x, y, width, height));
        drawn = false;
    }

    public boolean isDrawn() {
        return drawn;
    }

    public boolean isPointed(Point p) {
        return p.x >= this.x && p.x <= this.x + width &&
               p.y >= this.y && p.y <= this.y + height;
    }
}
