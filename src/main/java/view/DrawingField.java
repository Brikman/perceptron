package view;

import org.apache.commons.lang3.ArrayUtils;

import java.awt.*;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Stream;

public class DrawingField {
    private Graphics2D graphics2D;
    private Pixel[][]  pixels;
    private int n;
    private int size;

    public DrawingField(Graphics graphics, int size, int n) {
        this.graphics2D = (Graphics2D) graphics;
        this.size = size;
        this.n = n;
        this.pixels = new Pixel[n][n];
        int step = size / n;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                pixels[j][i] = new Pixel(i * step, j * step, step, step);
            }
        }
    }

    public double[][] getData() {
        double[][] bytes = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                bytes[i][j] = (byte) (pixels[i][j].isDrawn() ? 1.0 : 0.0);
            }
        }
        return bytes;
    }

    public void redrawAll() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (pixels[i][j].isDrawn()) {
                    pixels[i][j].draw(graphics2D);
                }
            }
        }
        drawGrid();
    }

    public void undrawAll() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                pixels[i][j].undraw(graphics2D);
            }
        }
        drawGrid();
    }

    public void draw(Point p) {
        onGrid:
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                Pixel pixel = pixels[i][j];
                if (pixel.isPointed(p) && !pixel.isDrawn()) {
                    pixel.draw(graphics2D);
                    break onGrid;
                }
            }
        }
        drawGrid();
    }

    public void undraw(Point p) {
        onGrid:
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                Pixel pixel = pixels[i][j];
                if (pixel.isPointed(p) && pixel.isDrawn()) {
                    pixel.undraw(graphics2D);
                    break onGrid;
                }
            }
        }
        drawGrid();
    }

    public void drawGrid() {
        int step = size / n;
        graphics2D.setColor(new Color(226, 232, 138));
        for (int i = 1; i < n; i++) {
            graphics2D.drawLine(i * step, 0, i * step, size);
            graphics2D.drawLine(0, i * step, size, i * step);
        }
    }
}
