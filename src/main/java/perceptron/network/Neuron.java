package perceptron.network;

import perceptron.functions.ActivationFunction;
import perceptron.functions.Functions;

public class Neuron {
    public final String ID;

    protected ActivationFunction activation;
    protected Layer layer;
    protected double shift = 0;

    protected double[] weights;

    protected double input  = 0;
    protected double output = 0;

    public Neuron(String id, Layer layer) {
        this(id, layer, null);
    }

    public Neuron(String id, Layer layer, ActivationFunction activation) {
        this(id, layer, activation, 0);
    }

    public Neuron(String id, Layer layer, ActivationFunction activation, double shift) {
        this.activation = activation != null ? activation : Functions.IDENTITY;
        this.shift = shift;
        this.layer = layer;
        this.ID = id;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public double[] getWeights() {
        return weights;
    }

    public double computeInput() {
        double[] input = layer.getInput();
        double sum = 0;
        for (int i = 0; i < weights.length; i++)
            sum += input[i] * weights[i];
        return this.input = sum + shift;
    }

    public double computeOutput() {
        return output = activation.value(computeInput());
    }

    public double getInput() {
        return input;
    }

    public double getOutput() {
        return output;
    }

    public void setShift(double shift) {
        this.shift = shift;
    }

    public double getShift() {
        return shift;
    }

    public ActivationFunction getActivation() {
        return activation;
    }
}
