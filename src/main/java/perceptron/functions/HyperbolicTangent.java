package perceptron.functions;

import static java.lang.Math.tanh;

public class HyperbolicTangent implements ActivationFunction {

    public double value(double x) {
        return tanh(x);
    }

    public double derivative(double x) {
        double f = value(x);
        return (1 - f) * (1 + f);
    }
}
