package perceptron.functions;

public class IdentityFunction implements ActivationFunction {

    public double value(double x) {
        return x;
    }

    public double derivative(double x) {
        return 1.0;
    }
}
