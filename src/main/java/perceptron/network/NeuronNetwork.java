package perceptron.network;

import perceptron.functions.ActivationFunction;
import perceptron.functions.Functions;

import java.util.*;

public class NeuronNetwork {
    private LearningAlgorithm algorithm;

    private InputLayer  inputLayer;
    private Layer       outputLayer;
    private List<Layer> hiddenLayers;

    public NeuronNetwork(
            int inputNeurons,
            int hiddenNeurons, int hiddenLayers,
            int outputNeurons,
            ActivationFunction activation,
            LearningAlgorithm algorithm) {
        createNetwork(inputNeurons, hiddenNeurons, hiddenLayers, outputNeurons, activation);
        this.algorithm = algorithm;
        this.algorithm.setNetwork(this);
    }

    private void createNetwork(int inputNeurons,
                               int hiddenNeurons, int hiddenLayersNumber,
                               int outputNeurons,
                               ActivationFunction activation) {

        inputLayer  = new InputLayer(inputNeurons);

        if (hiddenLayersNumber > 0) {
            hiddenLayers = new ArrayList<>(hiddenLayersNumber);
            for (int i = 0; i < hiddenLayersNumber; i++) {
                Layer linked = i == 0 ? inputLayer : hiddenLayers.get(i - 1);
                Layer layer = new Layer(hiddenNeurons, Functions.LOGISTIC_SIGMOID, linked);
                hiddenLayers.add(layer);
            }
            outputLayer = new Layer(outputNeurons, Functions.LOGISTIC_SIGMOID, hiddenLayers.get(hiddenLayersNumber - 1));
        } else {
            outputLayer = new Layer(outputNeurons, Functions.LOGISTIC_SIGMOID, inputLayer);
        }
    }

    public double[] learn(double[] input, double[] target) {
        return algorithm.learn(input, target);
    }

    public double[] test(double[] input) {
        inputLayer.setInput(input);
        inputLayer.compute();
        //System.out.println(inputLayer.getOutputs());

        if (hiddenLayers != null) {
            hiddenLayers.forEach(Layer::compute);
            //hiddenLayers.forEach(System.out::println);
        }

        outputLayer.compute();
        //System.out.println(outputLayer);

        return outputLayer.getOutput();
    }

    public InputLayer getInputLayer() {
        return inputLayer;
    }

    public Layer getOutputLayer() {
        return outputLayer;
    }

    public List<Layer> getHiddenLayers() {
        return hiddenLayers;
    }

    public List<Layer> getLayers() {
        List<Layer> layers = new ArrayList<>();
        layers.add(inputLayer);
        layers.addAll(hiddenLayers);
        layers.add(outputLayer);
        return layers;
    }
}
